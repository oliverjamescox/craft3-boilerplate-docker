module.exports = ({ env }) => ({
  plugins: [

    require('lost'),

    // Production only
    env === 'production' ? require('cssnano') : false,
    env === 'production' ? require('autoprefixer') : false
  ]
});