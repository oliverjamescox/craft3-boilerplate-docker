const webpack = require('webpack');
const path = require('path');

const merge = require('webpack-merge');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const common = require('./webpack.common.js');

module.exports = merge(common, {

    mode: 'development', 
    devtool: 'inline-source-map',
    watch: true,

    plugins: [
        new MiniCssExtractPlugin({
            filename: 'stylesheet.css'
            }),
    ],

    output: {
        path: path.resolve(__dirname, '../web/assets'),
        filename: 'App.js',
        publicPath: '/assets/'
    },

    module: {
        rules: [

            {   test: /\.js$/, 
                    exclude: /node_modules/, 
                    loader: "babel-loader" 
            },

            {
                test: /\.s?css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'postcss-loader',
                        options: {
                            sourceMap: true
                        }
                    },
                    {
                        loader: 'sass-loader',
                        options: {
                            sourceMap: true,
                        }
                    }
                ]
			},
        ]
      }

});