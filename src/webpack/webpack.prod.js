const webpack = require('webpack');
const merge = require('webpack-merge');
const path = require('path');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const TerserPlugin = require('terser-webpack-plugin');
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

const common = require('./webpack.common.js');

module.exports = merge(common, {

    mode: 'production',

    plugins: [
        new MiniCssExtractPlugin({
            filename: '[hash].css'
            }),

        // new BundleAnalyzerPlugin({
        //     analyzerMode: 'static',
        //     reportFilename: 'bundle-report.html',
        //     openAnalyzer: false
        // }),
    ],

    optimization: {
        minimizer: [
            new TerserPlugin()
        ]
      },

    output: {
        path: path.resolve(__dirname, '../web/assets'),
        filename: '[hash].js',
        publicPath: '/assets/'
    },

    module: {
        rules: [

            {   test: /\.js$/, 
                    exclude: /node_modules/, 
                    loader: "babel-loader" 
            },

            {
                test: /\.s?css$/,
                use: [
                    MiniCssExtractPlugin.loader,
                    {
                        loader: 'css-loader',
                    },
                    {
                        loader: 'postcss-loader',
                    },
                    {
                        loader: 'sass-loader',
                    }
                ]
			},
        ]
      }

});