import '../css/app.scss';

import objectFitImages from 'object-fit-images';
import PictureFill from 'picturefill';

PictureFill();
objectFitImages();

// Environment Check
if (process.env.NODE_ENV !== 'production') {
    console.log('Looks like we are in development mode!');
}